![Jekyll Version](https://img.shields.io/badge/Jekyll-3.1.2-red.svg)
![Build Status](https://gitlab.com/pages/jekyll/badges/master/build.svg)

----

Example [Jekyll] site using GitLab Pages. Read more at http://doc.gitlab.com/ee/pages/README.html

-----
# Theme: Jekyll 3 Default

# Original source:

This project was created with [Jekyll] v.3.1.2 default template by running `jekyll new project` locally. 
After that, the project was pushed to this repository with the following configurations:

- GitLab CI config: [`.gitlab-ci.yml`]
- Gitignore: [`.gitignore`]
- Jekyll config: [`_config.yml`]

# Building locally

_**Note:** We assume you already have [Jekyll 3.1.2][jek-312] installed and up and running on your computer._

To work locally with this project, there are a few options. But let's keep it simple:

- Fork, clone or download this project
- Adjust [`_config.yml`] according to your project
- Preview your project: `jekyll serve`

# GitLab User or Group Page

To use this project as your user/group website, you will need one additional step: just rename your project to `namespace.gitlab.io`, where `namespace` is your `username` or `groupname`. This can be done by navigating to `Project` -> `Settings`.

# Forked projects

If you forked this project for your own use, please go to `Project` -> `Settings` and remove the forking relationship, which won't be necessary in this case. 

Enjoy!

[Jekyll]: http://jekyllrb.com/
[jek-312]: https://rubygems.org/gems/jekyll/versions/3.1.2
[`_config.yml`]: https://gitlab.com/pages/jekyll/blob/master/_config.yml
[`.gitlab-ci.yml`]: https://gitlab.com/pages/jekyll/blob/master/.gitlab-ci.yml
[`.gitignore`]: https://gitlab.com/pages/jekyll/blob/master/.gitignore